import React, { Component } from 'react';
// import { HomeC, LoggedIn } from './config/Routes';
import { AsyncStorage, StatusBar } from 'react-native';
import { HomeC,LoggedIn } from '../config/routes';
export var token = '';
export default class Root extends Component {
  constructor() {
    super();
    this.state = {
      registered: false
    }
  }

  async getItem(key) {
    try {
      const value = await AsyncStorage.getItem(key);
      this.setState({ myKey: JSON.parse(value) });
      console.log("$$", this.state.myKey)
      if (this.state.myKey.response_code == "success") {
        this.setState({
          registered: !this.state.registered
        })
        token = this.state.myKey.access_token
      }
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
    return Promise.resolve(this.state.registered)
  }

  componentDidMount() {
    console.log("AWAIT:", this.getItem('loginResponse'))
  }

  render() {
    // register = this.state.registered
    // console.log("registered:", register);
    
    // return (this.state.registered) ? <HomeC /> : <LoggedIn />
    return <LoggedIn/>
  }

}



