import PropTypes from 'prop-types';
import React, { Component } from 'react';
// import styles from './SideMenu.style';
import { NavigationActions } from 'react-navigation';
import { Dimensions, Text, View, Image } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
var { height, width } = Dimensions.get('window');

export default class Drawer extends Component {
  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  }

  render() {
    return (
      <View style={styles.container}>
        <LinearGradient
          start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
          colors={['#FFEFBA', '#FFFFFF']}
          style={{
            height: height / 4.5,
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <Text style={{ color: '#800000', fontSize: height / 40, fontWeight: "600" }}>Mantra - The Design Studio</Text>
        </LinearGradient>
        <View>
          <View style={styles.navSectionStyle}>
            <Image source={require('../assets/images/drawer/home.png')}
              style={{
                resizeMode: 'contain',
                height: height / 20,
                width: height / 20,
                marginLeft: height / 25,
                alignSelf: 'center'
              }} />
            <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Home')}>
              Home
              </Text>
          </View>
          <View style={styles.navSectionStyle}>
            <Image source={require('../assets/images/drawer/order.png')}
              style={{
                resizeMode: 'contain',
                height: height / 20,
                width: height / 20,
                marginLeft: height / 25,
                alignSelf: 'center'
              }} />
            <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Order')}>
              Order
              </Text>
          </View>

          <View style={styles.navSectionStyle}>
            <Image source={require('../assets/images/drawer/task.png')}
              style={{
                resizeMode: 'contain',
                height: height / 20,
                width: height / 20,
                marginLeft: height / 25,
                alignSelf: 'center'
              }} />
            <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Task')}>
              Task
              </Text>
          </View>
          {/* <View style={styles.navSectionStyle}>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Page3')}>
                Task
              </Text>
            </View> */}
        </View>
      </View>
    );
  }
}

Drawer.propTypes = {
  navigation: PropTypes.object
};



const styles = {
  container: {
    paddingTop: 20,
    flex: 1
  },
  navItemStyle: {
    padding: 10,
    color: '#fff',
    marginLeft: width / 20

  },
  navSectionStyle: {
    backgroundColor: '#800000',
    height: height / 10,
    // justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    flexDirection: 'row'
  },
  sectionHeadingStyle: {
    paddingVertical: 10,
    paddingHorizontal: 5
  },
  footerContainer: {
    padding: 20,
    backgroundColor: 'lightgrey'
  }
};