import React, { Component } from 'react';
import { View, StatusBar, Text, Animated, Dimensions, Platform, TouchableOpacity, ScrollView, Image} from 'react-native';
import {Header, Title, Left, Right, Body} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import PercentageCircle from 'react-native-percentage-circle';
var {height, width} = Dimensions.get('screen');
export default class Home extends Component{
    constructor(){
        super();
        this.state = {

        }
    }


    render(){
        return(
            <View style={{backgroundColor: '#fff', height: height}}>
                <Header>
                    <LinearGradient
                            start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                            colors={['#FFEFBA', '#FFFFFF']}
                            style={{
                                width: width,
                                flexDirection:'row',
                                justifyContent:'space-evenly'
                            }}
                    >
                    <View style={{flexDirection: 'row', right:20, top: (Platform.OS == 'ios') ? 10 : 0}}>
                        <TouchableOpacity 
                            style={{
                                    right: width / 2.8, 
                                    // borderWidth:1
                                    }}
                            onPress={() => this.props.navigation.openDrawer()}>
                            <Image source={require('../assets/images/task/menu.png')} 
                                style={{        
                                        resizeMode: 'contain',
                                        height: height / 20,
                                        width: height / 20,
                                        marginLeft: height / 14,
                                        alignSelf: 'center'
                                    }} />
                        </TouchableOpacity>
                            <Text style={{
                                fontSize: 20,
                                right: width/18,
                                textAlign: 'center',
                                justifyContent:'center',
                                fontFamily:'Lato-Bold',
                            }}>Home</Text>
                        </View>
                    </LinearGradient>
                </Header>
                <View style={{borderWidth:1, height: height,justifyContent:'center', alignItems: 'center', flex:1}}>
                    <View style={{flexDirection:'row', justifyContent: 'space-around', borderWidth:1, width: width, height: height/2.8, alignItems: 'center'}}>
                      <PercentageCircle radius={60} percent={75} color={"#3498db"} borderWidth={20}/>
                      <PercentageCircle radius={60} percent={75} color={"#3498db"} borderWidth={20}/>
                    </View>
                    <View style={{height: height/20, borderWidth:1, width: width, backgroundColor: '#800000', alignItems: 'center', justifyContent: 'center'}}>
                      <Text style={{ fontFamily: "Lato-Bold", fontSize: height/38, color: "#FFFFFF" }}>Recent Activity</Text>
                    </View>
                    <ScrollView>
                    <TouchableOpacity style={{ marginTop: height / 40, marginLeft: width/30 }}>
                        <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 42, color: 'green', marginLeft: 40,alignSelf:'center'}}>Blue - green silk blouse	</Text>
                        <View style={{ flexDirection:'row',justifyContent: 'space-between',}}>
                            <View style={{ height: height / 12, width: height / 12, flexDirection: 'column', }}>
                                <Text style={{
                                        fontFamily: 'Lato-Regular', width: width / 2, fontSize:
                                            height / 52, color: 'rgba(0,0,0,0.87)', marginBottom: height / 500
                                    }}>Vaishnavi</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: ' rgba(0,0,0,0.32)', marginBottom: height / 500 }}>2019JAN00001</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'rgba(0,0,0,0.32)' }}>22/9/2019</Text>
                            </View>
                            <View style={{ width: width / 4, marginTop: height / 100}}>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'rgba(0,0,0,0.32)' }}>22/9/2019</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'green' }}>Pending</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'rgba(0,0,0,0.32)' }}>22/9/2019</Text>
                            </View>
                        </View>
                            <View style={{width: width/1.04,backgroundColor:'rgba(0,0,0,0.12)', height: height/400, alignSelf:'center', marginTop: height/70, right:5 }}/>
                    </TouchableOpacity>

                    <TouchableOpacity style={{ marginTop: height / 40, marginLeft: width/30 }}>
                        <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 42, color: 'green', marginLeft: 40,alignSelf:'center'}}>Blue - green silk blouse	</Text>
                        <View style={{ flexDirection:'row',justifyContent: 'space-between',}}>
                            <View style={{ height: height / 12, width: height / 12, flexDirection: 'column', }}>
                                <Text style={{
                                        fontFamily: 'Lato-Regular', width: width / 2, fontSize:
                                            height / 52, color: 'rgba(0,0,0,0.87)', marginBottom: height / 500
                                    }}>Vaishnavi</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: ' rgba(0,0,0,0.32)', marginBottom: height / 500 }}>2019JAN00001</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'rgba(0,0,0,0.32)' }}>22/9/2019</Text>
                            </View>
                            <View style={{ width: width / 4, marginTop: height / 100}}>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'rgba(0,0,0,0.32)' }}>22/9/2019</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'green' }}>Pending</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'rgba(0,0,0,0.32)' }}>22/9/2019</Text>
                            </View>
                        </View>
                            <View style={{width: width/1.04,backgroundColor:'rgba(0,0,0,0.12)', height: height/400, alignSelf:'center', marginTop: height/70, right:5 }}/>
                    </TouchableOpacity>

                    <TouchableOpacity style={{ marginTop: height / 40, marginLeft: width/30 }}>
                        <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 42, color: 'green', marginLeft: 40,alignSelf:'center'}}>Blue - green silk blouse	</Text>
                        <View style={{ flexDirection:'row',justifyContent: 'space-between',}}>
                            <View style={{ height: height / 12, width: height / 12, flexDirection: 'column', }}>
                                <Text style={{
                                        fontFamily: 'Lato-Regular', width: width / 2, fontSize:
                                            height / 52, color: 'rgba(0,0,0,0.87)', marginBottom: height / 500
                                    }}>Vaishnavi</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: ' rgba(0,0,0,0.32)', marginBottom: height / 500 }}>2019JAN00001</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'rgba(0,0,0,0.32)' }}>22/9/2019</Text>
                            </View>
                            <View style={{ width: width / 4, marginTop: height / 100}}>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'rgba(0,0,0,0.32)' }}>22/9/2019</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'green' }}>Pending</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'rgba(0,0,0,0.32)' }}>22/9/2019</Text>
                            </View>
                        </View>
                            <View style={{width: width/1.04,backgroundColor:'rgba(0,0,0,0.12)', height: height/400, alignSelf:'center', marginTop: height/70, right:5 }}/>
                    </TouchableOpacity>

                    <TouchableOpacity style={{ marginTop: height / 40, marginLeft: width/30 }}>
                        <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 42, color: 'green', marginLeft: 40,alignSelf:'center'}}>Blue - green silk blouse	</Text>
                        <View style={{ flexDirection:'row',justifyContent: 'space-between',}}>
                            <View style={{ height: height / 12, width: height / 12, flexDirection: 'column', }}>
                                <Text style={{
                                        fontFamily: 'Lato-Regular', width: width / 2, fontSize:
                                            height / 52, color: 'rgba(0,0,0,0.87)', marginBottom: height / 500
                                    }}>Vaishnavi</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: ' rgba(0,0,0,0.32)', marginBottom: height / 500 }}>2019JAN00001</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'rgba(0,0,0,0.32)' }}>22/9/2019</Text>
                            </View>
                            <View style={{ width: width / 4, marginTop: height / 100}}>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'rgba(0,0,0,0.32)' }}>22/9/2019</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'green' }}>Pending</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'rgba(0,0,0,0.32)' }}>22/9/2019</Text>
                            </View>
                        </View>
                            <View style={{width: width/1.04,backgroundColor:'rgba(0,0,0,0.12)', height: height/400, alignSelf:'center', marginTop: height/70, right:5 }}/>
                    </TouchableOpacity>

                    <TouchableOpacity style={{ marginTop: height / 40, marginLeft: width/30 }}>
                        <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 42, color: 'green', marginLeft: 40,alignSelf:'center'}}>Blue - green silk blouse	</Text>
                        <View style={{ flexDirection:'row',justifyContent: 'space-between',}}>
                            <View style={{ height: height / 12, width: height / 12, flexDirection: 'column', }}>
                                <Text style={{
                                        fontFamily: 'Lato-Regular', width: width / 2, fontSize:
                                            height / 52, color: 'rgba(0,0,0,0.87)', marginBottom: height / 500
                                    }}>Vaishnavi</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: ' rgba(0,0,0,0.32)', marginBottom: height / 500 }}>2019JAN00001</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'rgba(0,0,0,0.32)' }}>22/9/2019</Text>
                            </View>
                            <View style={{ width: width / 4, marginTop: height / 100}}>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'rgba(0,0,0,0.32)' }}>22/9/2019</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'green' }}>Pending</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'rgba(0,0,0,0.32)' }}>22/9/2019</Text>
                            </View>
                        </View>
                            <View style={{width: width/1.04,backgroundColor:'rgba(0,0,0,0.12)', height: height/400, alignSelf:'center', marginTop: height/70, right:5 }}/>
                    </TouchableOpacity>

                    <TouchableOpacity style={{ marginTop: height / 40, marginLeft: width/30 }}>
                        <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 42, color: 'green', marginLeft: 40,alignSelf:'center'}}>Blue - green silk blouse	</Text>
                        <View style={{ flexDirection:'row',justifyContent: 'space-between',}}>
                            <View style={{ height: height / 12, width: height / 12, flexDirection: 'column', }}>
                                <Text style={{
                                        fontFamily: 'Lato-Regular', width: width / 2, fontSize:
                                            height / 52, color: 'rgba(0,0,0,0.87)', marginBottom: height / 500
                                    }}>Vaishnavi</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: ' rgba(0,0,0,0.32)', marginBottom: height / 500 }}>2019JAN00001</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'rgba(0,0,0,0.32)' }}>22/9/2019</Text>
                            </View>
                            <View style={{ width: width / 4, marginTop: height / 100}}>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'rgba(0,0,0,0.32)' }}>22/9/2019</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'green' }}>Pending</Text>
                                <Text style={{ fontFamily: 'Lato-Regular', width: width / 2, fontSize: height / 52, color: 'rgba(0,0,0,0.32)' }}>22/9/2019</Text>
                            </View>
                        </View>
                            <View style={{width: width/1.04,backgroundColor:'rgba(0,0,0,0.12)', height: height/400, alignSelf:'center', marginTop: height/70, right:5 }}/>
                    </TouchableOpacity>
                </ScrollView>
                </View>
            </View>
        )
    }
}