import React, { Component } from 'react';
import { View, Dimensions, Image } from 'react-native';
import { createDrawerNavigator, createAppContainer, createSwitchNavigator, createStackNavigator } from 'react-navigation';
import Login from '../containers/LoginContainer';
import Order from '../containers/OrderContainer';
import Task from '../containers/TaskContainer';
import Home from '../containers/HomeContainer';
import Drawer from '../containers/DrawerContainer';
var { height, width } = Dimensions.get('window');
var rad = true;

export const HomeContainer = createDrawerNavigator({
  Home:{
    screen: Home,
    navigationOptions: {
      header: () => null
    }
  }, 
  Order: {
    screen: Order,
    navigationOptions: {
       header: () => null
    }
  },
  Task: {
    screen: Task,
    navigationOptions: {
      header: () => null
    }
  }
},{
  contentComponent: Drawer,
  drawerWidth: 300
});

const createRootNavigator = (signedIn = false) => {
  return createSwitchNavigator(
    {
      LogIn: {
        screen: HomeContainer
      },
      LogOut: {
        screen: Login
      }
    },
    {
      initialRouteName: signedIn ? "LogIn" : "LogOut",
    },
  );
};


export const LoggedIn = createAppContainer(createRootNavigator(false));
export const HomeC = createAppContainer(HomeContainer);